package com.letak.printing.quote_service.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

	@ExceptionHandler(ServiceProcessingException.class)
	void handleBadRequests(ServiceProcessingException bre, HttpServletResponse response) throws IOException {

		int respCode = (bre.errCode == ServiceProcessingException.ID_NOT_FOUND) ? HttpStatus.NOT_FOUND.value()
				: HttpStatus.BAD_REQUEST.value();

		response.sendError(respCode, bre.errCode + ":" + bre.getMessage());
	}
}
