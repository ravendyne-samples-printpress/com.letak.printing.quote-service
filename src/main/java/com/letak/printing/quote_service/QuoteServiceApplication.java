package com.letak.printing.quote_service;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.letak.printing.quote_service.config.MyApplicationContextInitializer;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCaching
@EntityScan(basePackageClasses = {
        com.letak.printing.domain_model.Package.class, 
        com.letak.printing.quote_service.domain.Package.class
        })
public class QuoteServiceApplication {

	public static void main(String[] args) {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(QuoteServiceApplication.class);

        builder.initializers(new MyApplicationContextInitializer());

        builder.run(args);
	}
    
    @Bean // Create restTemplate bean to be used by this client code
    @LoadBalanced // Tells Spring to use the Ribbon load balancer (which is in the one that is in the classpath)
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
