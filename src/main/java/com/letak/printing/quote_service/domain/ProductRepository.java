package com.letak.printing.quote_service.domain;

import org.springframework.data.repository.CrudRepository;

import com.letak.printing.domain_model.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {

}
