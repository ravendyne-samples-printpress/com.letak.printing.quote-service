package com.letak.printing.quote_service.domain;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.domain_model.entity.ProductParameter;

public interface ProductParameterRepository extends CrudRepository<ProductParameter, Integer> {

//    @Cacheable("parameterByProduct")
    List<ProductParameter> findByProduct(Product product);
}
