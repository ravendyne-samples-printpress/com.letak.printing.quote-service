package com.letak.printing.quote_service.domain;

import org.springframework.data.repository.CrudRepository;

import com.letak.printing.domain_model.entity.QuoteScript;

public interface QuoteScriptRepository extends CrudRepository<QuoteScript, Integer> {

}
