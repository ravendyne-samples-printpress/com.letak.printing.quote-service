package com.letak.printing.quote_service.data;

import java.util.Map;

public class ScriptParameters {
    
    public static final String PARAMS_NAME = "params";

    public static class ParamValue {
        private String defaultValue;
        private String[] values;

        public ParamValue() {
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        public String[] getValues() {
            return values;
        }

        public void setValues(String[] values) {
            this.values = values;
        }
    }

    Integer productId;

    Map<String, String> attributes;

    Map<String, ParamValue> parameters;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public Map<String, ParamValue> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, ParamValue> parameters) {
        this.parameters = parameters;
    }
}
