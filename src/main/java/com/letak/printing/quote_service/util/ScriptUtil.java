package com.letak.printing.quote_service.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.domain_model.entity.ProductParameter;
import com.letak.printing.domain_model.entity.ProductParameterValue;
import com.letak.printing.quote_service.data.ScriptParameters;
import com.letak.printing.quote_service.data.ScriptParameters.ParamValue;

public class ScriptUtil {

    public static ScriptParameters createScriptParams(Product product, List<ProductParameter> productParams, Map<String, String> productAttributeValues) {
        
        Map<String, ParamValue> productParameters = convertProductParameters(productParams);
        
        ScriptParameters parameters = new ScriptParameters();
        parameters.setProductId(product.getId());
        parameters.setAttributes(productAttributeValues);
        parameters.setParameters(productParameters);
        
        return parameters;
    }

    public static Map<String, ParamValue> convertProductParameters(List<ProductParameter> productParams) {

        Map<String, ParamValue> productParameters = new HashMap<>();

        for( ProductParameter param : productParams ) {
            String paramScriptName = param.getScriptName();
            if(paramScriptName == null || paramScriptName.isEmpty()) {
                paramScriptName = param.getName();
            }
            String paramValue = param.getDefaultValue();
            if(paramValue == null) {
                paramValue = "";
            }

            ParamValue value = new ParamValue();
            value.setDefaultValue(paramValue);
            List<String> paramValueList = new ArrayList<>();
            for(ProductParameterValue pv: param.getProductParameterValues()) {
                String scriptValue = pv.getScriptValue();
                if(scriptValue == null) {
                    scriptValue = pv.getValue();
                }
                paramValueList.add(scriptValue);
            }
            value.setValues(paramValueList.toArray(new String[0]));

            productParameters.put(paramScriptName, value);
        }

        return productParameters;
    }

}
