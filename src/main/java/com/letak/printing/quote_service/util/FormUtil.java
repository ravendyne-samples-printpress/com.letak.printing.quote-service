package com.letak.printing.quote_service.util;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.letak.printing.domain_model.dto.order.AttributeSelection;
import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.domain_model.entity.ProductAttribute;
import com.letak.printing.domain_model.entity.ProductAttributeValue;
import com.letak.printing.quote_service.data.FormData;

public class FormUtil {

    private static Log logger = LogFactory.getLog(FormUtil.class);
    
    public static final String FORM_DATA_PRODUCT_ID = "product";
    public static final String FORM_DATA_ORDER_FIELD_PREFIX = "order.";

    public static Map<String, String> convertToMap(List<FormData> data) {

        Map<String, String> attributeValues = new HashMap<>();

        for (FormData entry : data) {

            String attributeId = entry.getName();
            String attributeValueId = entry.getValue();
            
            attributeValues.put(attributeId, attributeValueId);
        }
        return attributeValues;
    }

    public static Integer stripProductId(List<FormData> data) {

        Integer productId = null;

        for(Iterator<FormData> iterator = data.iterator(); iterator.hasNext(); ) {
            FormData entry = iterator.next();
            
            if(entry.getName().equals(FORM_DATA_PRODUCT_ID)) {
                productId = Integer.parseInt(entry.getValue());
                iterator.remove();
                break;
            }
        }

        return productId;
    }

    public static Map<String, String> stripOrderInfo(List<FormData> data) {

        Map<String, String> orderInfo = new HashMap<>();

        for (Iterator<FormData> iterator = data.iterator(); iterator.hasNext();) {
            FormData formData = iterator.next();
            if (formData.getName().startsWith(FORM_DATA_ORDER_FIELD_PREFIX)) {

                orderInfo.put(formData.getName().substring(FORM_DATA_ORDER_FIELD_PREFIX.length()), formData.getValue());

                iterator.remove();
            }
        }

        return orderInfo;
    }

    public static List<AttributeSelection> decodeAttributes(Map<String, String> attributeValues, Product product) {

        List<AttributeSelection> attrValuesList = new ArrayList<>();

        List<ProductAttribute> attributes = product.getProductAttributes();
        
        
        for( ProductAttribute attribute : attributes ) {

            String attrValueValue = attributeValues.get( attribute.getId().toString() );
            if( attrValueValue == null ) {
                attrValueValue = attributeValues.get( attribute.getScriptName() );
                if( attrValueValue == null ) {
                    attrValueValue = attributeValues.get( attribute.getName() );
                }
            }
            if( attrValueValue == null ) {
                // unknown attribute, will not end up in attrs map
                logger.warn("Unknown attribute '" + attrValueValue + "' for pid = " + product.getId() + " ('" + product.getName() + "')");
                continue;
            }


            // both attribute values and ids can be integers
            // so we first check if there's an attribute with value that matches what we've got
            ProductAttributeValue value = getAttributeValueForValue(attribute.getProductAttributeValues(), attrValueValue);
            if( value == null ) {
                // let's try id next
                Integer attrValueId = getIntValue(attrValueValue);
                if(attrValueId != null) {
                    value = getAttributeValueForId(attribute.getProductAttributeValues(), attrValueId);
                }

                if(attrValueId == null && value == null) {
                    logger.error(
                            "Can't find attribute value with value or id = '" + attrValueValue +
                            "' for attribute with id = " + attribute.getId() + " ('" + attribute.getName() +
                            "') for pid = " + product.getId() + " ('" + product.getName() + "')");
//                    throw new ServiceProcessingException(ServiceProcessingException.ID_BAD_REQUEST, "invalid attribute value");
                    continue;
                }
            }
            
            attrValuesList.add(new AttributeSelection(attribute, value));
        }

        return attrValuesList;
    }

    public static Map<String, String> convertForScript(List<AttributeSelection> attrValuesList) {

        Map<String, String> scriptAttributesParameter = new HashMap<>();
        
        for( AttributeSelection attrValue : attrValuesList ) {
            
            String attributeScriptName = attrValue.getAttribute().getScriptName();
            if(attributeScriptName == null) {
                attributeScriptName = attrValue.getAttribute().getName();
            }
            String attributeScriptValue = attrValue.getValue().getScriptValue();
            if(attributeScriptValue == null) {
                attributeScriptValue = attrValue.getValue().getValue();
            }

            scriptAttributesParameter.put(attributeScriptName, attributeScriptValue);
            
        }

        return scriptAttributesParameter;
    }

    private static Integer getIntValue(String source) {

        ParsePosition pos = new ParsePosition(0);

        Number result = NumberFormat.getIntegerInstance().parse(source, pos);

        if( source.length() == pos.getIndex() ) {
            return new Integer( result.intValue() );
        }

        return null;
    }

    private static ProductAttributeValue getAttributeValueForValue(List<ProductAttributeValue> productAttributeValues, String attrValue) {
        
        for(ProductAttributeValue value : productAttributeValues) {
            if(value.getScriptValue() != null && value.getScriptValue().equals(attrValue)) {
                return value;
            }
            if(value.getValue().equals(attrValue)) {
                return value;
            }
        }
        
        return null;
    }

    private static ProductAttributeValue getAttributeValueForId(List<ProductAttributeValue> productAttributeValues, Integer id) {
        
        for(ProductAttributeValue value : productAttributeValues) {
            if(value.getId().equals(id)) {
                return value;
            }
        }
        
        return null;
    }

}
