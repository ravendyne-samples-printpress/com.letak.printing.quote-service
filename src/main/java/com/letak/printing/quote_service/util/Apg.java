package com.letak.printing.quote_service.util;

import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/*
 * based off of APG utility
 * randpass.c - Random password generation module of PWGEN program
 * Copyright (c) 1999, 2000, 2001, 2002, 2003
 * Adel I. Mirzazhanov. All rights reserved
 * 
 */

public class Apg {
    public static final int APG_MAX_PASSWORD_LENGTH = 128;
    
    public static enum Type {
        NUMERIC,
        SPECIAL,
        CAPITAL,
        SMALL,
        RESTRICTED
    }

    private static class Symbol {
        char ch;
        Type type;

        public Symbol(char ch, Type type) {
            this.ch = ch;
            this.type = type;
        }

        public Symbol(int ch, Type type) {
            this.ch = (char) ch;
            this.type = type;
        }
    };

    static Symbol smbl[] = {
            new Symbol('a', Type.SMALL), new Symbol('b', Type.SMALL), new Symbol('c', Type.SMALL), new Symbol('d', Type.SMALL),
            new Symbol('e', Type.SMALL), new Symbol('f', Type.SMALL), new Symbol('g', Type.SMALL), new Symbol('h', Type.SMALL),
            new Symbol('i', Type.SMALL), new Symbol('j', Type.SMALL), new Symbol('k', Type.SMALL), new Symbol('l', Type.SMALL),
            new Symbol('m', Type.SMALL), new Symbol('n', Type.SMALL), new Symbol('o', Type.SMALL), new Symbol('p', Type.SMALL),
            new Symbol('q', Type.SMALL), new Symbol('r', Type.SMALL), new Symbol('s', Type.SMALL), new Symbol('t', Type.SMALL),
            new Symbol('u', Type.SMALL), new Symbol('v', Type.SMALL), new Symbol('w', Type.SMALL), new Symbol('x', Type.SMALL),
            new Symbol('y', Type.SMALL), new Symbol('z', Type.SMALL), new Symbol('A', Type.CAPITAL), new Symbol('B', Type.CAPITAL),
            new Symbol('C', Type.CAPITAL), new Symbol('D', Type.CAPITAL), new Symbol('E', Type.CAPITAL), new Symbol('F', Type.CAPITAL),
            new Symbol('G', Type.CAPITAL), new Symbol('H', Type.CAPITAL), new Symbol('I', Type.CAPITAL), new Symbol('J', Type.CAPITAL),
            new Symbol('K', Type.CAPITAL), new Symbol('L', Type.CAPITAL), new Symbol('M', Type.CAPITAL), new Symbol('N', Type.CAPITAL),
            new Symbol('O', Type.CAPITAL), new Symbol('P', Type.CAPITAL), new Symbol('Q', Type.CAPITAL), new Symbol('R', Type.CAPITAL),
            new Symbol('S', Type.CAPITAL), new Symbol('T', Type.CAPITAL), new Symbol('U', Type.CAPITAL), new Symbol('V', Type.CAPITAL),
            new Symbol('W', Type.CAPITAL), new Symbol('X', Type.CAPITAL), new Symbol('Y', Type.CAPITAL), new Symbol('Z', Type.CAPITAL),
            new Symbol('1', Type.NUMERIC), new Symbol('2', Type.NUMERIC), new Symbol('3', Type.NUMERIC), new Symbol('4', Type.NUMERIC),
            new Symbol('5', Type.NUMERIC), new Symbol('6', Type.NUMERIC), new Symbol('7', Type.NUMERIC), new Symbol('8', Type.NUMERIC),
            new Symbol('9', Type.NUMERIC), new Symbol('0', Type.NUMERIC), new Symbol(33, Type.SPECIAL), new Symbol(34, Type.SPECIAL),
            new Symbol(35, Type.SPECIAL), new Symbol(36, Type.SPECIAL), new Symbol(37, Type.SPECIAL), new Symbol(38, Type.SPECIAL),
            new Symbol(39, Type.SPECIAL), new Symbol(40, Type.SPECIAL), new Symbol(41, Type.SPECIAL), new Symbol(42, Type.SPECIAL),
            new Symbol(43, Type.SPECIAL), new Symbol(44, Type.SPECIAL), new Symbol(45, Type.SPECIAL), new Symbol(46, Type.SPECIAL),
            new Symbol(47, Type.SPECIAL), new Symbol(58, Type.SPECIAL), new Symbol(59, Type.SPECIAL), new Symbol(60, Type.SPECIAL),
            new Symbol(61, Type.SPECIAL), new Symbol(62, Type.SPECIAL), new Symbol(63, Type.SPECIAL), new Symbol(64, Type.SPECIAL),
            new Symbol(91, Type.SPECIAL), new Symbol(92, Type.SPECIAL), new Symbol(93, Type.SPECIAL), new Symbol(94, Type.SPECIAL),
            new Symbol(95, Type.SPECIAL), new Symbol(96, Type.SPECIAL), new Symbol(123, Type.SPECIAL), new Symbol(124, Type.SPECIAL),
            new Symbol(125, Type.SPECIAL), new Symbol(126, Type.SPECIAL)
    };

    public static String gen_rand_pass(int minl, int maxl, Set<Type> pass_mode) {

        if (minl > APG_MAX_PASSWORD_LENGTH || maxl > APG_MAX_PASSWORD_LENGTH || minl < 1 || maxl < 1 || minl > maxl)
            return new String();

        int length = randint(minl, maxl + 1);
        char[] password_string = new char[length];

        int number_of_symbols = smbl.length;
        int[] random_weight = new int[number_of_symbols];
        int max_weight = 0;
        int max_weight_element_number = 0;

        for (int str_index = 0; str_index < length; str_index++) {

            for (int j = 0; j < number_of_symbols; j++) {

                random_weight[j] = 0;
            }

            /* Assign random weight in weight array if mode is present */
            for (int j = 0; j < number_of_symbols; j++) {

                if ( pass_mode.contains( smbl[j].type ) && !pass_mode.contains(Type.RESTRICTED) ) {

                    random_weight[j] = randint(1, 20000);
                }
            }

            /* Find an element with maximum weight */
            for (int j = 0; j < number_of_symbols; j++) {

                if (random_weight[j] > max_weight) {

                    max_weight = random_weight[j];

                    max_weight_element_number = j;
                }
            }

            /* Get password symbol */
            password_string[str_index] = smbl[max_weight_element_number].ch;

            max_weight = 0;
            max_weight_element_number = 0;
        }

        return new String(password_string);
    }

    private static int randint(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);
    }

    public static void main(String[] args) {

//        EnumSet<Type> pass_mode = EnumSet.of(Type.NUMERIC, Type.CAPITAL, Type.SMALL, Type.SPECIAL);
        EnumSet<Type> pass_mode = EnumSet.of(Type.CAPITAL, Type.SMALL);

        System.out.println(gen_rand_pass(7, 7, pass_mode));
        System.out.println(gen_rand_pass(6, 6, EnumSet.of(Type.NUMERIC)));
        System.out.println(gen_rand_pass(7, 7, pass_mode));
        System.out.println(gen_rand_pass(6, 6, EnumSet.of(Type.NUMERIC)));
        System.out.println(gen_rand_pass(7, 7, pass_mode));
        System.out.println(gen_rand_pass(6, 6, EnumSet.of(Type.NUMERIC)));
    }
}
