package com.letak.printing.quote_service.service;

import java.io.IOException;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.letak.printing.domain_model.dto.order.AttributeSelection;
import com.letak.printing.domain_model.dto.order.PrintPressOrderDetailsDto;
import com.letak.printing.domain_model.entity.PrintPressOrder;
import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.quote_service.data.FormData;
import com.letak.printing.quote_service.domain.PrintPressOrderRepository;
import com.letak.printing.quote_service.domain.ProductRepository;
import com.letak.printing.quote_service.util.Apg;
import com.letak.printing.quote_service.util.Apg.Type;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@RestController
public class OrderController {

    private static Log logger = LogFactory.getLog(OrderController.class);

    @Autowired
    QuoteService quoteService;
    
    @Autowired
    MailingService mailingService;
    
    @Autowired
    ProductRepository productRepository;
    
    @Autowired
    PrintPressOrderRepository printPressOrderRepository;
    
    @Autowired
    Configuration freemarkerConfiguration;


    @RequestMapping(value = {"/order"}, method = RequestMethod.POST)
    ServiceReponse handleFileUpload(@RequestParam("data") String dataString, @RequestParam(value = "file", required = false) MultipartFile file) {
        ServiceReponse response = new ServiceReponse();
        response.setResultOk();
        
        ObjectMapper mapper = new ObjectMapper();

        TypeReference<List<FormData>> mapType = new TypeReference<List<FormData>>() {
        };

        try {

            byte[] attachmentBytes = null;

            List<FormData> data = mapper.readValue(dataString, mapType);
            Map<String, Object> quoteData = quoteService.calculate(data);

            if ( file != null && !file.isEmpty() )
            {
                try
                {
                    attachmentBytes = file.getBytes();
                    response.setMessage("Attachment received");
                }
                catch (Exception e)
                {
                    response.setMessage("Upload of attachment failed => " + e.getMessage());
                }
            }
            else
            {
                response.setMessage("No file to upload.");
            }
            


            @SuppressWarnings("unchecked")
            Map<String, String> orderInfoMap = (Map<String, String>) quoteData.get(QuoteService.RESULT_ORDER_INFO);
            @SuppressWarnings("unchecked")
            List<AttributeSelection> attrValuesList = (List<AttributeSelection>) quoteData.get(QuoteService.RESULT_ORDER_ATTRIBUTES);

            PrintPressOrderDetailsDto orderDto = PrintPressOrderDetailsDto.createDto(orderInfoMap);
            orderDto.setUrlSlug( generateSlug() );
            String orderNumber = generateOrderNumber();
            orderDto.setOrderNumber( orderNumber );
            attrValuesList = AttributeSelection.sanitize(attrValuesList);
            orderDto.setAttributeSelections(attrValuesList);

            Integer productId = (Integer) quoteData.get(QuoteService.RESULT_ORDER_PRODUCT);
            
            Optional<Product> productOpt = productRepository.findById(productId);

            if( productOpt.isPresent()) {
                
                PrintPressOrder order = PrintPressOrderDetailsDto.createEntity( orderDto );
                order.setPrintPress(productOpt.get().getPrintPress());
                order.setAttachment(attachmentBytes);

                order = printPressOrderRepository.save(order);
                
                String recipient = orderDto.getEmail();
                if(recipient != null && (recipient.equals("dev-test-pp@yahoo.com") || recipient.equals("dev-test-pp@gmail.com"))) {

                    try {

                        Template emailTemplate = freemarkerConfiguration.getTemplate("order-confirmation-email.ftl");
                        
                        String emailText = FreeMarkerTemplateUtils.processTemplateIntoString(emailTemplate, orderDto);
                        
//                        mailingService.send(orderNumber, emailText, recipient);
                        
                        System.out.println(emailText);
    
                    } catch (TemplateException e) {
    
                        logger.error("Order confirmation email not sent for order no. '" + orderNumber +"': template processing exception", e);
                    }
                }

            } else {

                logger.error("!! BUG !! : No product for ID '" + productId +"'");
                response.setResultError();
            }

            response.setData(orderDto);

        } catch (IOException e) {
            
            logger.error("Couldn't decode form data '" + dataString +"'", e);

            response.setMessage("Invalid form data");
            response.setResultError();
        }

        return response;
    }

    private String generateSlug() {
        return Apg.gen_rand_pass(7, 7, EnumSet.of(Type.CAPITAL, Type.SMALL));
    }

    private String generateOrderNumber() {
        return Apg.gen_rand_pass(6, 6, EnumSet.of(Type.NUMERIC));
    }
}
