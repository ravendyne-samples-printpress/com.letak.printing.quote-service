package com.letak.printing.quote_service.service;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MailingService {
    
    private static Log logger = LogFactory.getLog(MailingService.class);

    @Value("${quote-service.mail.host:none}")
    String host;

    @Value("${quote-service.mail.port:none}")
    String port;

    @Value("${quote-service.mail.user:none}")
    String user;

    @Value("${quote-service.mail.pass:none}")
    String pass;

    @Value("${quote-service.mail.from:none}")
    String from;
    

    public void send(String orderNumber, String messageText, String recipient) {

        if( user.equals("none") || pass.equals("none") || from.equals("none") ) {
            logger.error("Not sending email: mail user credentials not defined");
            return;
        }
        if( host.equals("none") || port.equals("none") ) {
            logger.error("Not sending email: mail host parameters are not defined");
            return;
        }


        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", port);

        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(user, pass);
            }
        });

        try {

            Message message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse( recipient ));
            message.setSubject("Your order confirmation and summary");

            message.setContent(messageText, "text/html");

            Transport.send(message);

            logger.info("Sent order confirmation email for order no. '" + orderNumber + "' to '" + recipient + "'");

        } catch (MessagingException e) {

            logger.error("Error sending order confirmation email for order no. '" + orderNumber + "' to '" + recipient + "'", e);
        }
    }
}
