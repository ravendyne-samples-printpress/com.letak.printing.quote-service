package com.letak.printing.quote_service.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.letak.printing.quote_service.data.FormData;

@RestController
public class QuoteController {

    private static Log logger = LogFactory.getLog(QuoteController.class);

    @Autowired
    QuoteService quoteService;

    @RequestMapping(value = {"/getquote"}, method = RequestMethod.POST)
    ServiceReponse getQuote(@RequestBody List<FormData> data) {
        ServiceReponse response = new ServiceReponse();

        Map<String, Object> resultData = quoteService.quote(data);

        response.setResultOk();
        response.setData(resultData);

        return response;
    }
}
