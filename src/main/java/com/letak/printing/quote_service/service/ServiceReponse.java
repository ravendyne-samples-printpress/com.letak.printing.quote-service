package com.letak.printing.quote_service.service;

public class ServiceReponse {
    public static final String OK = "ok";
    public static final String ERROR = "error";

    private String result;
    private String message;
    private Object data;

    public String getResult() {
        return result;
    }

    public void setResultOk() {
        this.result = OK;
    }

    public void setResultError() {
        this.result = ERROR;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
