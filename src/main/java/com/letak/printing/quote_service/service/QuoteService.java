package com.letak.printing.quote_service.service;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.letak.printing.domain_model.dto.order.AttributeSelection;
import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.quote_service.data.FormData;
import com.letak.printing.quote_service.data.ScriptParameters;
import com.letak.printing.quote_service.domain.ProductParameterRepository;
import com.letak.printing.quote_service.domain.QuoteScriptRepository;
import com.letak.printing.quote_service.exception.ServiceProcessingException;
import com.letak.printing.quote_service.util.FormUtil;
import com.letak.printing.quote_service.util.ScriptUtil;

import bsh.EvalError;
import bsh.Interpreter;

@Service
public class QuoteService {
    
    public static final String RESULT_QUANTITY = "komada";
    public static final String RESULT_ORDER_PRODUCT = "order.product";
    public static final String RESULT_ORDER_INFO = "order.info";
    public static final String RESULT_ORDER_ATTRIBUTES = "order.attributes";
    public static final String RESULT_QUOTE = "quote";
    public static final String RESULT_UNIT_PRICE = "unit_price";

    private static Log logger = LogFactory.getLog(QuoteService.class);
    
//  @Value("${env}")
    private String env = "";

    @Autowired
    QuoteScriptRepository quoteScriptRepository;
    
    @Autowired
    ProductParameterRepository productParameterRepository;

    @Autowired
    private RestTemplate rTemplate;

    private static final boolean doRealWork = true;

    public Map<String, Object> quote(List<FormData> data) {

        Map<String, Object> result = calculate(data);
        
        result.remove(RESULT_ORDER_PRODUCT);
        result.remove(RESULT_ORDER_ATTRIBUTES);
        
        return result;
    }

    public Map<String, Object> calculate(List<FormData> data) {
        Integer productId = FormUtil.stripProductId(data);
        
        if(productId == null) {
            logger.error("Form data missing product id field '" + FormUtil.FORM_DATA_PRODUCT_ID + "'");
            throw new ServiceProcessingException(ServiceProcessingException.ID_BAD_REQUEST, "invalid product id");
        }

        Product product = rTemplate.getForObject("http://ADMIN-SERVICE/" + env + "product/" + productId, Product.class);
        if(product == null) {
            logger.error("No product for id = " + productId);
            throw new ServiceProcessingException(ServiceProcessingException.ID_BAD_REQUEST, "invalid product id");
        }


        Map<String, String> orderInfo = FormUtil.stripOrderInfo(data);
        Map<String, String> attributesMap = FormUtil.convertToMap(data);

        // changes attributesMap from <int,int> to actual <name,value> pairs where needed
        List<AttributeSelection> attrValuesList = FormUtil.decodeAttributes( attributesMap, product );
        attributesMap = FormUtil.convertForScript( attrValuesList );
        
        if( product.getProductScripts().isEmpty() ) {
            logger.error("Missing script for pid = " + product.getId() + " ('" + product.getName() + "')");
            throw new ServiceProcessingException(ServiceProcessingException.ID_BAD_REQUEST, "missing qoute script");
        }

        ScriptParameters params = ScriptUtil.createScriptParams(product, productParameterRepository.findByProduct(product), attributesMap);
        
        Double quote = -1.0;
        Double unit_price = -1.0;
        if(doRealWork) {
            quote = calculatePrice(params, product.getProductScripts().get(0).getScriptSource());
            unit_price = quote;

            String komada = params.getAttributes().get(RESULT_QUANTITY);
            if(komada != null) {
                unit_price = quote / Double.parseDouble( komada );
            }
        }


        Map<String, Object> resultData = new HashMap<>();
        
        resultData.put(RESULT_ORDER_PRODUCT, productId);
        resultData.put(RESULT_ORDER_INFO, orderInfo);
        resultData.put(RESULT_ORDER_ATTRIBUTES, attrValuesList);

        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        resultData.put(RESULT_QUOTE, decimalFormat.format(quote));
        resultData.put(RESULT_UNIT_PRICE, decimalFormat.format(unit_price));

        return resultData;
    }

    private Double calculatePrice(ScriptParameters params, String script) {
        
        Double quote = -1.0;

        try {
            Interpreter ipr = new Interpreter();
            ipr.eval("importCommands(\"/scripts/lib\")");
            ipr.set(ScriptParameters.PARAMS_NAME, params);
            
//            InputStream is = getClass().getResourceAsStream("/scripts/quote_11.bsh");
//            BufferedReader buffer = new BufferedReader(new InputStreamReader(is));
//            ipr.eval(buffer);
            ipr.eval(script);
            
            Object result = ipr.get(RESULT_QUOTE);
    
//            System.out.println("result = '" + result + "'");
//            System.out.println("result type = '" + result.getClass().getName() + "'");

            quote = (Double)result;

        } catch (EvalError ee) {
            throw new ServiceProcessingException(1, ee.getMessage());
        }

        return quote;
    }

}
