package com.letak.printing.quote_service.config;

import java.util.Random;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

public class MyRandomValuePropertySource extends PropertySource<Random> {

	private static Log logger = LogFactory.getLog(MyRandomValuePropertySource.class);
	private static final String PREFIX = "myrandom.";
	private static final String NAME = "myrandom";
	private static final String PORT = "port";
	
	private static final int RANGE_START = 20000;
	private static final int RANGE_END = 40000;
	private static int randomStaticPort = RANGE_START + new Random().nextInt(RANGE_END - RANGE_START);

	public MyRandomValuePropertySource(String name) {
		super( name, new Random() );
	}

	@Override
	public Object getProperty(String name) {

		if (!name.startsWith(PREFIX)) {
			return null;
		}

		if (logger.isTraceEnabled()) logger.trace("Asking for '" + name + "'");

		final String localName = name.substring(PREFIX.length());

		if (localName.equals(PORT)) {
			if (logger.isTraceEnabled()) logger.trace("Providing '" + randomStaticPort + "' value for '" + name + "'");
			return randomStaticPort;
		}

//		if (localName.startsWith(PORT) && localName.length() > PORT.length() + 1) {
//			final String range = localName.substring(PORT.length() + 1, localName.length() - 1);
//			return getNextInRange(range);
//		}

//		final byte[] bytes = new byte[32];
//		getSource().nextBytes(bytes);
//		return DigestUtils.md5DigestAsHex(bytes);
		
		return null;
	}

//	private int getNextInRange(String range) {
//		String[] tokens = StringUtils.commaDelimitedListToStringArray(range);
//		Integer rangeStart = Integer.valueOf(tokens[0]);
//
//		if (tokens.length == 1) {
//			return getSource().nextInt(rangeStart);
//		}
//
//		int rangeEnd = Integer.valueOf(tokens[1]);
//
//		return rangeStart + getSource().nextInt(rangeEnd - rangeStart);
//	}

	public static void addToEnvironment(ConfigurableEnvironment environment) {
		environment.getPropertySources().addAfter(
				StandardEnvironment.SYSTEM_ENVIRONMENT_PROPERTY_SOURCE_NAME,
				new MyRandomValuePropertySource(NAME));
		logger.trace("MyRandomValuePropertySource add to Environment");
	}

}
