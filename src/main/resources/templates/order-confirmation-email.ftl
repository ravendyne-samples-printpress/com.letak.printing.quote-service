<!DOCTYPE html>
<html lang="${lang!'en'}">
<head>
<meta charset="UTF-8">
</head>
<body>
<h2>Thank you for your order!</h2>
<p>Here is the summary:</p>

<span class="template.order_summary">

<#--
    <h5 class="card-title">Order No. <a href="//url/to/order/summary/view/${urlSlug}">${orderNumber}</a></h5>
-->
    <h5 class="card-title">Order No. ${orderNumber}</h5>

    <div class="row">

        <div class="col-md-12">
            <h5 class="card-title">Opcije</h5>
        </div>

        <span class="col-md-12 row template.order_selections">

            <span class="template.order_selection_item">
            <#list attributeSelections as attr>
            <div class="col-md-4">
                <label>${attr.attribute.name}</label>
            </div>
            <div class="col-md-8">
                ${attr.value.value}
            </div>
            </#list>
            </span>

        </span>

        <div class="col-md-12">
            <h5 class="card-title">Podaci za dostavu / preuzimanje</h5>
        </div>

        <div class="col-md-4">
            <label>Ime i prezime</label>
        </div>
        <div class="col-md-8">
            ${name!''}
        </div>

        <div class="col-md-4">
            <label>Adresa</label>
        </div>
        <div class="col-md-8">
            ${address!''}
        </div>

        <div class="col-md-4">
            <label>Telefon</label>
        </div>
        <div class="col-md-8">
            ${phone!''}
        </div>

        <div class="col-md-4">
            <label>Ime firme</label>
        </div>
        <div class="col-md-8">
            ${comapny!''}
        </div>

        <div class="col-md-4">
            <label>E-mail</label>
        </div>
        <div class="col-md-8">
            ${email}
        </div>

        <div class="col-md-4">
            <label>Graficka priprema</label>
        </div>
        <div class="col-md-8">
            yes/no
        </div>

        <div class="col-md-4">
            <label>Napomena</label>
        </div>
        <div class="col-md-8">
            ${note!''}
        </div>

    </div>

</span>

<body>
</html>
